Rails.application.routes.draw do

	mount ActionCable.server => '/cable'
	devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout' }, controllers: { sessions: 'sessions' }
	#get '/login', to: 'sessions#new'
	#post '/login', to: 'sessions#create'
	#delete '/logout', to: 'sessions#destroy'
	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	root to: "rooms#index"
	resources :users
	resources :rooms
	resources :rooms do
		resources :messages
	end
end
