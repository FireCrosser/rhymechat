require 'rails_helper'

describe ::PoetryChecker do

  subject{ ::PoetryChecker.new }

  context '#get_translation' do

    it 'should get correct word translation' do
      expect(subject.send(:get_translation, 'EXTORTING')).to eq('EH0 K S T AO1 R T IH0 NG')
    end

    it 'should raise ArgumentError if word is empty' do
      expect{subject.send(:get_translation, '')}.to raise_error(ArgumentError, 'Word cannot be empty')
    end
  end

  context '#get_translated_line' do
    it 'should get correct translated line' do
      expect(subject.get_translated_line('Hey, man!')).to eq('HH EY1+M AE1 N')
    end

    it 'should return empty line if empty line was as input' do
      expect(subject.get_translated_line('')).to eq('')
    end
  end
end
