class PoetryChecker < LineChecker

	def check(first, second)
	end

  def get_translated_line(line)
    #empty_line = line.downcase.gsub(/[a-z0-9-\s]/i
    line_words = line.gsub(/[^a-z0-9\s]/i, '').split()
    @translated_line = ''
    splitter = ''
    line_words.each do |w|
      @translated_line << splitter + get_translation(w)
      splitter = '+'
    end
    @translated_line
  end

  def get_translation(word)
    if word.length == 0
      raise ArgumentError, 'Word cannot be empty'
    end
    uword = word.upcase
    @translation = Dictionary.where(word: uword).take.translation
  end

  private :get_translation

end
