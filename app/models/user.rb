class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
	has_many :messages

	def appear(page = nil)
		user = get_db_user
		if user
			if !page
				user.first.increment!(:appearances)
			elsif user.page != page
				user.update(page: page)
			end
			user.first.increment!(:appearances)
		else
			OnlineUser.new(user_id: self.id).save
		end
	end

	def disappear
		user = get_db_user
		if user
			#user.first.decrement!(:appearances)
			user.destroy
		end
	end

	def away
		user = get_db_user
		if user
			user.destroy
		end
	end

	def is_online
		if cookies.signed[:user_id] = self.id
			return true
		else
			user = get_db_user
			if user 
				return true
			else
				return false
			end
		end	
	end

	private

	def get_db_user
		user = OnlineUser.find_by(user_id: self.id)
		return user
	end

end
