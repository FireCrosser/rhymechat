class Dictionary < ApplicationRecord

  scope :by_id, -> (id) { where(id: id) }
  scope :by_word, -> (word) { where(word: word) }
end
