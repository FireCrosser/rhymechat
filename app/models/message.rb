class Message < ApplicationRecord
	belongs_to :user
	belongs_to :room

	validates :text, presence: true

	after_commit { RoomWorker.perform_async(self.id) }

	scope :by_id, -> (id) { where(id: id) }
	scope :by_room_id, -> (room_id) { where(room_id: room_id) }
	scope :with_user, -> { includes(:user) }
	scope :order_by_creation, -> { order(created_at: :asc) }

	def is_acceptable
		last_message = Message
			.by_room_id(self.room_id)
			.order_by_creation
			.last
	end
end
