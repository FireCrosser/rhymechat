class RoomWorker
  include Sidekiq::Worker

  def perform(message_id)
    message = Message.by_id(message_id).with_user.take
    ActionCable.server.broadcast "rooms:#{message.room_id}",
      message: 
      MessagesController.render(
        partial: 'messages/message',
        locals: { message: message }
      )
  end
end
