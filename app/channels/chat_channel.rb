class ChatChannel < ApplicationCable::Channel

	def subscribed
		stream_from 'rooms/{params[:room_id]}'
	end

end
