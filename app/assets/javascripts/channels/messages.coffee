App.messages = App.cable.subscriptions.create {
	channel: "RoomChannel"
	room_id: $('#room_id').val()
},

  process_data: (data) ->
	  mc = $('.messages-container')
	  mc.append(data.message)
	  mc.stop().animate({
		  scrollTop: mc[0].scrollHeight
	  }, 800);

  connected: ->
	  @followCurrentMessage()
	  #@installPageChangeCallback()
	  console.log "connected"

  received: (data) ->
	  console.log "Some data received"
	  @process_data(data)

  userIsCurrentUser: (comment) ->
    $(comment).attr('data-user-id') is $('meta[name=current-user]').attr('id')

  followCurrentMessage: ->
      @perform 'follow', room_id: 1

  installPageChangeCallback: ->
	unless @installedPageChangeCallback
	  @installedPageChangeCallback = true
	  $(document).on 'turbolinks:load', -> App.messages.followCurrentMessage()
		
