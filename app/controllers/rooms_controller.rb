class RoomsController < ApplicationController
	before_action :authenticate_user!
	layout "chatroom", only: :show

	def index
		@rooms = Room.all
	end

	def show
		room_id = params[:id]
		@room = Room.find(room_id)
		@messages = Message.by_room_id(room_id).order_by_creation.with_user
		@new_message = Message.new
	end
end
