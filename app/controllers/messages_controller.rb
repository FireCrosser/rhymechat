class MessagesController < ApplicationController

  def index
    @messages = Message.joins(:user)
  end


  def create
    @new_message = Message.new
    @new_message.attributes = message_params
    @new_message.user_id = current_user.id
    @user = @new_message.user
    @new_message.room_id = params[:room_id]
    @new_message.save
    render layout: false
  end

  private
  def message_params
    params.require(:message).permit(:text)
  end
end
