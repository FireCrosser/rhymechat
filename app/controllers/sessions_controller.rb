class SessionsController < Devise::SessionsController
  layout "login", only: :new

  def create
    logger.debug "USER ID: #{current_user.id}"
    cookies.signed[:user_id] = current_user.id
    super
  end

  def destroy
    cookies.delete :user_id
    super
  end
end
