# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170328155023) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dictionary", force: :cascade do |t|
    t.string "word"
    t.text   "translation"
    t.index ["word"], name: "index_dictionary_on_word", unique: true, using: :btree
  end

  create_table "message", force: :cascade do |t|
    t.string   "text",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id",    null: false
    t.integer  "room_id",    null: false
    t.index ["user_id"], name: "index_message_on_user_id", using: :btree
  end

  create_table "online_user", force: :cascade do |t|
    t.integer "user_id",                 null: false
    t.text    "page"
    t.integer "appearances", default: 1, null: false
    t.index ["user_id"], name: "index_online_user_on_user_id", unique: true, using: :btree
  end

  create_table "privilege", force: :cascade do |t|
    t.string "CreateRoom"
  end

  create_table "room", force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.index ["name"], name: "room_name_key", unique: true, using: :btree
  end

  create_table "user", force: :cascade do |t|
    t.string   "username"
    t.string   "email",               default: "", null: false
    t.string   "encrypted_password",  default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.index ["email"], name: "index_user_on_email", unique: true, using: :btree
  end

  add_foreign_key "message", "\"user\"", column: "user_id"
  add_foreign_key "online_user", "\"user\"", column: "user_id"
end
