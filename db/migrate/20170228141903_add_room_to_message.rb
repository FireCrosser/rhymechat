class AddRoomToMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :message, :room_id, :integer
  end
end
