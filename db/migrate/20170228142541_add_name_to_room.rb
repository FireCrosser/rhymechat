class AddNameToRoom < ActiveRecord::Migration[5.0]
  def change
    add_column :room, :name, :string, :null => false
	add_index :room, :name, :unique => true
  end
end
