class AddPasswordToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :user, :password, :string
  end
end
