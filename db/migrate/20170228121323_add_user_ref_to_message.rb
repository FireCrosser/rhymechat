class AddUserRefToMessage < ActiveRecord::Migration[5.0]
  def change
    add_reference :message, :user, foreign_key: true
  end
end
