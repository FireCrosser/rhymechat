require 'active_record'
class ImportDictionary < ActiveRecord::Migration[5.0]
  def up
	  columns = [:word, :translation]
	  # Unzip cmudict.zip in dicts folder
	  dict = read_dictionary('dicts/cmudict.txt')
	  Dictionary.import columns, dict
  end

  def down
	  Dictionary.destroy_all
	  execute('ALTER SEQUENCE dictionary_id_seq RESTART;');
  end

  def read_dictionary(file)
	  dictionary = []
	  File.open(file, 'r') do |f|
		  f.each_line do |l|
			  begin
				  row = l.split(' ', 2)
				  row[1] = row[1].strip
				  dictionary << row
			  rescue ArgumentError => msg
				  puts msg
				  puts l
				  l.each_byte do |x|
					  puts x
				  end
			  end
		  end
	  end
	  return dictionary
  end
end
