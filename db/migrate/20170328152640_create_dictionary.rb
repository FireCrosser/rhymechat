class CreateDictionary < ActiveRecord::Migration[5.0]
  def change
    create_table :dictionary do |t|
      t.string :word
      t.text :translation
    end

	  add_index :dictionary, :word, unique: true
  end
end
