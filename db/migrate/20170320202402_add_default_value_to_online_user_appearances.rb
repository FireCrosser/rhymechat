class AddDefaultValueToOnlineUserAppearances < ActiveRecord::Migration[5.0]
	def change
		change_column_default :online_user, :appearances, 0
	end
end
