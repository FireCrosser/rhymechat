class CreateOnlineUser < ActiveRecord::Migration[5.0]
  def change
    create_table :online_user do |t|
      t.integer :user_id, null: false, index: { unique: true }
      t.text :page
	  t.integer :appearances, null: false
    end
	add_foreign_key :online_user, :user
  end
end
